 /*   
 * Project: OSMP
 * FileName: ServerException.java
 * version: V1.0
 */
package com.osmp.netty.exception;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2017年2月7日 上午11:20:46上午10:51:30
 */
public class ClientException extends RuntimeException{
	
	private static final long serialVersionUID = 4898359170164795003L;
	private final long msgId;

	public long getMsgId() {
		return msgId;
	}

	public ClientException (long msgId, final Exception cause){
		super(cause.getMessage(), cause);
		this.msgId = msgId;
	}
	
}
